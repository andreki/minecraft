#!/bin/bash

defaultJar=jars/c0.0.11a.jar
jar=$1

# Check if a parameter was provided
if [ -z "$1" ]; then
    echo "Drag a minecraft jar into this script to run it"
    echo "Using default jar ${defaultJar}"
    jar=$defaultJar
#    exit 1
fi

java -cp "${jar}:lwjgl-2.9.3/jar/*" -Djava.library.path=lwjgl-2.9.3/native/macosx/ com.mojang.rubydung.RubyDung
java -cp "${jar}:lwjgl-2.9.3/jar/*" -Djava.library.path=lwjgl-2.9.3/native/macosx/ com.mojang.minecraft.RubyDung
java -cp "${jar}:lwjgl-2.9.3/jar/*" -Djava.library.path=lwjgl-2.9.3/native/macosx/ com.mojang.minecraft.Minecraft
java -cp "${jar}:lwjgl-2.9.3/jar/*" -Djava.library.path=lwjgl-2.9.3/native/macosx/ net.minecraft.client.Minecraft

# Wait for a keypress
read -p "Press any key to continue..."
